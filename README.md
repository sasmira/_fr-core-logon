# FoundryVTT - fr-core-logon
Fork du core FR avec logon réduit/compact

## Installation

Pour l'installation, merci de suivre les instructions suivantes :

1.  Ouvrer Foundry, aller dans l'onglet "Add-on Modules" cliquer sur "Install module".
2.  Copier l'URL suivante : https://gitlab.com/sasmira/_fr-core-logon/-/raw/main/module/module.json
3.  Dans le champ "Manifest URL" Coller l'URL ci-dessus.
4.  Cliquer sur "Install" et attendre que l'installation soit complète.
5.  Aller dans l'onglet "Configuration" de foundry et sélectionner dans "Default Language", la langue comme ci-dessous.
<img src="https://puu.sh/Jy4pe/7b52f6dcbe.png">

6.  Cliquer sur "Save Changes" en bas à droite et redémarrarer Foundry.

7.  Lors de l'ouverture de votre monde vous aurez un logon réduit comme ci-dessous.
<img src="https://puu.sh/Jy4p5/214c04314b.png">
<img src="https://puu.sh/Jy4oy/801517e986.jpg">
