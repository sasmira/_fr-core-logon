# Patch Notes
## Compatibility
- Ready for v12 version. Based on FR-CORE versions and follow them.



## core-12.331.5.1
- Réduction de l'opacité de la fenetre l'accueil lorsque la souris n'est pas dessus.

## core-12.331.5
- Maj traduction FR

## core-12.331.2
- Maj traduction FR
- Maj v12

## core-12.330.3
- Maj traduction FR
- Maj v12

## core-12.328.1
- Maj traduction FR
- Maj v12

## core-11.315.2
- Maj traduction FR

## core-11.311.1
- Maj traduction FR

## core-11.306.2
- Mise à jour du code pour la v11

## core-11.306.1
- Mise à jour du code pour la v11

## core-11.299.1
- Maj traduction FR

## core-10.288.2
- Maj traduction FR

## core-9.242.0
Initial release
